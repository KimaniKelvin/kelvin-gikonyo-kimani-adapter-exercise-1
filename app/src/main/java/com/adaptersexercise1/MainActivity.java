package com.adaptersexercise1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = findViewById(R.id.spinner);

        //add iems to spinner
        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("-- select desired program --");
        arrayList.add("BSc. Computer Science");
        arrayList.add("BSc. Applied Computer Science");
        arrayList.add("BSc. IT");
        arrayList.add("Diploma in Computer Science");
        arrayList.add("MSc. Computer Science");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayList);
        spinner.setAdapter(arrayAdapter);
    }
}